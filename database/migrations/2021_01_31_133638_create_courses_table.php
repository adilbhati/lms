<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('course_name');
            $table->string('course_title');
            $table->string('course_description');
            $table->bigInteger('course_author')->unsigned();
            $table->foreign('course_author')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('course_image');
            $table->boolean('course_isPremium')->default(0);
            $table->integer('course_price')->nullable();
            $table->bigInteger('course_institute')->unsigned();//
            $table->foreign('course_institute')->references('institute_id')->on('institutes')->onDelete('cascade')->onUpdate('cascade');
            $table->string('course_category');//
            $table->string('course_educationLevel');//
            $table->string('course_semester');//    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
