<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstitutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institutes', function (Blueprint $table) {
            $table->id('institute_id');
            $table->string('institute_name');
            $table->string('institute_email')->unique();
            //$table->string('institute_password');
            $table->string('institute_description');
            $table->string('institute_image');
            $table->string('institute_courseType');//
            $table->bigInteger('institute_courses')->unsigned();//
            $table->foreign('institute_courses')->references('id')->on('courses')->onDelete('cascade')->onUpdate('cascade');//
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institutes');
    }
}
