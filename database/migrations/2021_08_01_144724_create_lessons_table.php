<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->id('lesson_id');
            $table->string('lesson_name');
            $table->string('lesson_duration');
            $table->string('lesson_description');
            $table->string('lesson_image');
            $table->string('lesson_document');
            $table->string('lesson_video_url');
            $table->string('lesson_transcript','10000');
            $table->bigInteger('lesson_course')->unsigned();
            $table->foreign('lesson_course')->references('id')->on('courses')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('lesson_author')->unsigned();
            $table->foreign('lesson_author')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}
