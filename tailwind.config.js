module.exports = {
  purge: [

    './resources/**/*.blade.php',

    './resources/**/*.js',

    './resources/**/*.vue',

  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'custom-yellow':'#BAA333',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
