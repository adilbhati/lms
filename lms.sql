-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 20, 2021 at 06:58 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lms`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `course_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_author` bigint(20) UNSIGNED NOT NULL,
  `course_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_isPremium` tinyint(1) NOT NULL DEFAULT 0,
  `course_price` int(11) DEFAULT NULL,
  `course_institute` bigint(20) UNSIGNED NOT NULL,
  `course_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_educationLevel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_semester` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course_name`, `course_title`, `course_description`, `course_author`, `course_image`, `course_isPremium`, `course_price`, `course_institute`, `course_category`, `course_educationLevel`, `course_semester`, `created_at`, `updated_at`) VALUES
(1, 'Learn to Build AI assistant', 'Learn to Build AI assistant like JARVIS using Python', 'Hi There', 1, 'Learn to Build AI assistant.jpg', 0, NULL, 2, 'CS', 'Bsc CS', '1st', '2021-08-15 09:30:12', '2021-08-15 09:30:12'),
(2, 'Adobe Illustrator CC', 'Complete Course in Adobe Illustrator CC', 'ABC', 1, 'Adobe Illustrator CC.jpg', 0, NULL, 2, 'IT', 'Bsc IT', '1st', '2021-08-17 10:29:27', '2021-08-17 10:29:27'),
(4, 'The Complete CSS course', 'The Complete CSS course', 'Drastically Improve Your CSS Knowledge', 1, 'The Complete CSS course.jpg', 1, 499, 2, 'IT,CS', 'Bsc CS', 'NA', '2021-08-20 09:30:56', '2021-08-20 09:30:56');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `institutes`
--

CREATE TABLE `institutes` (
  `institute_id` bigint(20) UNSIGNED NOT NULL,
  `institute_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `institute_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `institute_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `institute_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `institute_courseType` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `institute_courses` bigint(20) UNSIGNED NOT NULL,
  `institute_created` timestamp NULL DEFAULT NULL,
  `institute_updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `institutes`
--

INSERT INTO `institutes` (`institute_id`, `institute_name`, `institute_email`, `institute_description`, `institute_image`, `institute_courseType`, `institute_courses`, `institute_created`, `institute_updated`) VALUES
(2, 'No Institute', 'admin@edudev.com', 'This is for students who are not affilated to an Institute', 'No Institute.png', 'BscIT, BscCS', 1, '2021-08-14 06:29:12', '2021-08-14 06:29:12');

-- --------------------------------------------------------

--
-- Table structure for table `lessons`
--

CREATE TABLE `lessons` (
  `lesson_id` bigint(20) UNSIGNED NOT NULL,
  `lesson_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lesson_duration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lesson_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lesson_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lesson_document` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lesson_video_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lesson_transcript` varchar(10000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lesson_course` bigint(20) UNSIGNED NOT NULL,
  `lesson_author` bigint(20) UNSIGNED NOT NULL,
  `lesson_created` timestamp NULL DEFAULT NULL,
  `lesson_updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lessons`
--

INSERT INTO `lessons` (`lesson_id`, `lesson_name`, `lesson_duration`, `lesson_description`, `lesson_image`, `lesson_document`, `lesson_video_url`, `lesson_transcript`, `lesson_course`, `lesson_author`, `lesson_created`, `lesson_updated`) VALUES
(1, 'Create Your Conversational AI Assistant', '10 mins', 'Create Your Conversational AI Assistant', 'Create Your Conversational AI Assistant.jpg', 'Create Your Conversational AI Assistant.pdf', 'https://www.youtube.com/embed/1lwddP0KUEg', 'Hello there', 1, 1, '2021-08-18 11:10:08', '2021-08-18 11:10:08'),
(2, 'Basic CSS Syntax', '3 min', 'In this lecture, we are going to talk about what is the use of the CSS and the syntax of the CSS', '', '', 'Edsxf_NBFrw', 'In this lecture, we are going to talk about what is the use of the CSS and the syntax of the CSS', 4, 1, '2021-08-20 09:53:15', '2021-08-20 09:53:15'),
(3, 'inline and internal styling', 'inline and internal styling', 'In this lecture, we are going to talk about inline and internal CSS styling. We will learn what are the benefits of using these methods. Moreover, we will talk about the disadvantages and leads to why we need external styling.', 'inline and internal styling.jpg', 'inline and internal styling.pdf', '1PnVor36_40', 'In this lecture, we are going to talk about inline and internal CSS styling. We will learn what are the benefits of using these methods. Moreover, we will talk about the disadvantages and leads to why we need external styling.', 4, 1, '2021-08-20 10:24:25', '2021-08-20 10:24:25');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_01_11_152545_add_roles_to_users_table', 1),
(5, '2021_01_31_133638_create_courses_table', 1),
(6, '2021_02_21_050311_create_institutes_table', 1),
(7, '2021_03_17_154614_create_student_course_table', 1),
(8, '2021_04_11_063234_create_roles_table', 1),
(9, '2021_07_25_172408_create_user_institute_table', 1),
(10, '2021_08_01_144724_create_lessons_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2021-08-14 06:30:27', '2021-08-14 06:30:27'),
(2, 'Institute Admin', '2021-08-14 06:30:27', '2021-08-14 06:30:27'),
(3, 'Moderator', '2021-08-14 06:30:57', '2021-08-14 06:30:57'),
(4, 'Student', '2021-08-14 06:30:57', '2021-08-14 06:30:57');

-- --------------------------------------------------------

--
-- Table structure for table `student_course`
--

CREATE TABLE `student_course` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_course`
--

INSERT INTO `student_course` (`user_id`, `course_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-08-19 09:55:25', '2021-08-19 09:55:25'),
(1, 4, '2021-08-20 10:27:43', '2021-08-20 10:27:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `email_verified_at`, `password`, `remember_token`, `designation`, `created_at`, `updated_at`, `role`) VALUES
(1, 'Adil', 'adil@gmail.com', 'AdilB', NULL, '$2y$10$NO239d1LQuriUU/0juiTW.3EFJTVneeiO.nRIFy9Qo4P6qMdqQU.2', NULL, '', '2021-08-14 01:02:05', '2021-08-14 01:02:05', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_institute`
--

CREATE TABLE `user_institute` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `institute_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_institute`
--

INSERT INTO `user_institute` (`user_id`, `institute_id`, `created_at`, `updated_at`) VALUES
(1, 2, '2021-08-14 01:02:05', '2021-08-14 01:02:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `courses_course_author_foreign` (`course_author`),
  ADD KEY `course_institute` (`course_institute`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `institutes`
--
ALTER TABLE `institutes`
  ADD PRIMARY KEY (`institute_id`),
  ADD UNIQUE KEY `institutes_institute_email_unique` (`institute_email`),
  ADD KEY `institutes_institute_courses_foreign` (`institute_courses`);

--
-- Indexes for table `lessons`
--
ALTER TABLE `lessons`
  ADD PRIMARY KEY (`lesson_id`),
  ADD KEY `lessons_lesson_course_foreign` (`lesson_course`),
  ADD KEY `lessons_lesson_author_foreign` (`lesson_author`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `student_course`
--
ALTER TABLE `student_course`
  ADD KEY `student_course_user_id_foreign` (`user_id`),
  ADD KEY `student_course_course_id_foreign` (`course_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `role` (`role`);

--
-- Indexes for table `user_institute`
--
ALTER TABLE `user_institute`
  ADD KEY `user_institute_user_id_foreign` (`user_id`),
  ADD KEY `institute_id` (`institute_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `institutes`
--
ALTER TABLE `institutes`
  MODIFY `institute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lessons`
--
ALTER TABLE `lessons`
  MODIFY `lesson_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_course_author_foreign` FOREIGN KEY (`course_author`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`course_institute`) REFERENCES `institutes` (`institute_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lessons`
--
ALTER TABLE `lessons`
  ADD CONSTRAINT `lessons_lesson_author_foreign` FOREIGN KEY (`lesson_author`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lessons_lesson_course_foreign` FOREIGN KEY (`lesson_course`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `student_course`
--
ALTER TABLE `student_course`
  ADD CONSTRAINT `student_course_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `student_course_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role`) REFERENCES `roles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_institute`
--
ALTER TABLE `user_institute`
  ADD CONSTRAINT `user_institute_ibfk_1` FOREIGN KEY (`institute_id`) REFERENCES `institutes` (`institute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_institute_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
