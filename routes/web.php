<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\Admin\AdminCourseController;
use App\Http\Controllers\Admin\AdminInstituteController;
use App\Http\Controllers\Admin\AdminLessonController;
use App\Http\Controllers\ProfileController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::post('/logout', [LogoutController::class, 'store'])->name('logout');

Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class, 'store']);

Route::get('/register', [RegisterController::class, 'index'])->name('register');
Route::post('/register', [RegisterController::class, 'store']);

Route::get('manage/dashboard', [DashboardController::class, 'index'])->name('dashboard');

Route::get('frontend/courses', [CourseController::class,'index'])->name('course');
Route::get('frontend/courses/create', [CourseController::class, 'create'])->name('create-course')->middleware(['AdminRole']);
Route::post('/frontend/courses/create',[CourseController::class,'store'])->name('store-course')->middleware(['AdminRole']);
Route::get('/frontend/courses/show/{id}',[CourseController::class,'show'])->name('show-course');
Route::get('/frontend/courses/edit/{id}',[CourseController::class,'edit'])->name('edit-course')->middleware(['AdminRole']);
Route::put('/frontend/courses/update/{id}',[CourseController::class,'update'])->name('update-course')->middleware(['AdminRole']);
Route::delete('/frontend/courses/show/{id}',[CourseController::class,'destroy'])->name('delete-course')->middleware(['AdminRole']);
//Student Subscribe
Route::post('/frontend/courses/show/{id}',[CourseController::class,'subscribe'])->name('subscribe-course');
//Route::get('/frontend/courses/show/{id}',[CourseController::class,'checkSubscription'])->name('checkSubscription');


Route::get('frontend/profile/institutes',[ProfileController::class,'instituteIndex']);
Route::get('frontend/profile/institutes/show/{id}',[ProfileController::class,'showInstitute']);
Route::get('frontend/profile/user/show/{id}',[ProfileController::class,'showUser']);
Route::get('frontend/profile/user/create',[ProfileController::class,'createUser']);
Route::post('frontend/profile/user/create',[ProfileController::class, 'storeUser'])->name('storeUser');

Route::resource('/manage/courses', AdminCourseController::class);
Route::resource('/manage/institutes', AdminInstituteController::class);


Route::get('manage/changerole', [DashboardController::class, 'changeRole'])->name('changeRole');
Route::post('manage/changerole', [DashboardController::class, 'changeUserRole'])->name('changeUserRole');
//Route::resource('frontend/courses', CourseController::class);

Route::get('manage/add-lesson',[AdminLessonController::class,'addLesson'])->name('add-lesson');
Route::post('manage/add-lesson',[AdminLessonController::class,'storeLesson'])->name('save-lesson');
Route::get('frontend/lessons/show/{id}',[AdminLessonController::class,'getLesson'])->name('view-lesson');
Route::delete('/manage/courses/{id}',[AdminLessonController::class,'destroyLesson'])->name('delete-lesson')->middleware(['AdminRole']);

Route::get('/check',function(){
    return 'Hey There';
})->middleware(['AdminRole']);