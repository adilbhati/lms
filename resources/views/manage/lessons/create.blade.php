@extends('layouts.adminapp')

@section('content')
    <div class="flex justify-center">
        <div class="w-8/12 bg-white p-6 rounded-lg">
            <h3>Add Lesson</h3>
            <form action="{{ route('save-lesson') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="mb-4">                    
                    <label for="lesson_course" class="sr-only">Course Name</label>
                    <select name="lesson_course" class="form-control" required>
                        <option value="">Select Course</option>
                        @foreach ($coursestoshow as $coursetoshow)
                            <option value="{{ $coursetoshow->id }}">{{ $coursetoshow->course_name }}</option>
                        @endforeach
                    </select>

                    @error('lesson_course')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-4 mt-4">
                    <label for="lesson_name" class="sr-only">Lesson Name</label>
                    <input type="text" name="lesson_name" id="lesson_name" placeholder="Lesson name" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('lesson_name') border-red-500 @enderror" value="{{ old('lesson_name') }}">

                    @error('lesson_name')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-4 mt-4">
                    <label for="lesson_duration" class="sr-only">Lesson Duration (mins)</label>
                    <input type="text" name="lesson_duration" id="lesson_duration" placeholder="Lesson Duration" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('lesson_duration') border-red-500 @enderror" value="{{ old('lesson_duration') }}">

                    @error('lesson_duration')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-4 mt-4">
                    <label for="lesson_description" >Lesson Description</label>
                    <textarea name="lesson_description" id="course_descsription" placeholder="Lesson Description" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('lesson_description') border-red-500 @enderror" value="{{ old('lesson_description') }}">@error('lesson_description')<div class="text-red-500 mt-2 text-sm">{{ $message }}</div>@enderror</textarea>
                </div>
                
                <p>Lesson Image</p>
                <div class="mt-4 mb-4">
                    <label for="lesson_image" class="sr-only">Image</label>                    
                    <input type="file" placeholder="Lesson Image" name="lesson_image" id="lesson_image" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('lesson_image') border-red-500 @enderror"/>
                    
                    @error('lesson_image')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                <p>Lesson Document</p>
                <div class="mb-4 mt-4">
                    <label for="lesson_document" class="sr-only">Document</label>
                    <input type="file" placeholder="Lesson Image" name="lesson_document" id="lesson_document" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('lesson_document') border-red-500 @enderror"/>
                    
                    @error('lesson_document')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-4 mt-4">
                    <label for="lesson_video_url" class="sr-only">Lesson url</label>
                    <input type="text" name="lesson_video_url" id="lesson_video_url" placeholder="Lesson url" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('lesson_video_url') border-red-500 @enderror" value="{{ old('lesson_video_url') }}">

                    @error('lesson_video_url')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-4 mt-4">
                    <label for="lesson_transcript" >Lesson Transcript</label>
                    <textarea name="lesson_transcript" id="course_dtranscript" placeholder="Lesson transcript" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('lesson_transcript') border-red-500 @enderror" value="{{ old('lesson_transcript') }}">@error('lesson_transcript')<div class="text-red-500 mt-2 text-sm">{{ $message }}</div>@enderror</textarea>
                </div>
                
                
                <div>
                    <input type="submit" class="bg-blue-500 text-white px-4 py-3 rounded font-medium items-center" value="Create Lesson">
                </div>
            </form>

        </div>
    </div>

    <script type="text/javascript">
        
    </script>
@endsection
