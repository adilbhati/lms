@extends('layouts.adminapp')

@section('content')
    <div class="flex justify-center">
        <div class="w-8/12 bg-white p-6 rounded-lg">
            <h3>Add Institute</h3>
            <form action="{{ route('institutes.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="mb-4 mt-4">
                    <label for="institute_name" class="sr-only">Institute Name</label>
                    <input type="text" name="institute_name" id="institute_name" placeholder="Institute name" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('institute_name') border-red-500 @enderror" value="{{ old('institute_name') }}">

                    @error('institute_name')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                <div class="mb-4 mt-4">
                    <label for="institute_email" class="sr-only">Institute Email</label>
                    <input type="email" name="institute_email" id="institute_email" placeholder="Institute email" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('institute_email') border-red-500 @enderror" value="{{ old('institute_email') }}">

                    @error('institute_email')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>                

                <div class="mb-4">
                    <label for="institute_description" >Institute Description</label>
                    <textarea name="institute_description" id="institute_descsription" placeholder="Institute Description" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('institute_description') border-red-500 @enderror" value="{{ old('institute_description') }}">
                    @error('institute_description')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                    </textarea>
                </div>

                <div class="mb-4">
                    <label for="institute_image" class="sr-only">Logo</label>
                    <input type="file" name="institute_image" id="institute_image" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('institute_image') border-red-500 @enderror"/>
                    
                    @error('institute_image')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>                

                <div class="mb-4">
                    <label for="institute_courseType" class="sr-only">Programs Offered</label>
                    <input type="text" name="institute_courseType" id="institute_courseType" placeholder="Courses Offered" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('institute_courseType') border-red-500 @enderror" value="{{ old('institute_courseType') }}">

                    @error('institute_courseType')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                {{--<div class="mb-4">
                    <label for="institute_courses" class="sr-only">Courses Offered</label>
                    <input type="text" name="institute_courses" id="institute_courses" placeholder="Courses Offered" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('institute_courses') border-red-500 @enderror" value="{{ old('institute_courses') }}">

                    @error('institute_courses')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>--}}

                <div>
                    <input type="submit" class="bg-blue-500 text-white px-4 py-3 rounded font-medium items-center" value="Add Institute">
                </div>
            </form>

        </div>
    </div>    
@endsection
