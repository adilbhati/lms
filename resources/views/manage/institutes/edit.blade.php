@extends('layouts.adminapp')

@section('content')
    <div class="flex justify-center">
        <div class="w-8/12 bg-white p-6 rounded-lg">
            <h3>Edit Course</h3>
            <form action="{{ route('institutes.update', $institute->institute_id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <div class="mb-4 mt-4">
                    <label for="institute_name" class="sr-only">Institute Name</label>
                    <input type="text" name="institute_name" id="institute_name" placeholder="Institute name" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('institute_name') border-red-500 @enderror" value="{{ old('institute_name',$institute->institute_name) }}">

                    @error('institute_name')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>                

                <div class="mb-4">
                    <label for="institute_description" >Institute Description</label>
                    <textarea name="institute_description" id="institute_descsription" placeholder="Institute Description" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('institute_description') border-red-500 @enderror" value="{{ old('institute_description',$institute->institute_description) }}">@error('institute_description')<div class="text-red-500 mt-2 text-sm">{{ $message }}</div>@enderror</textarea>
                </div>

                <div class="mb-4">
                    <label for="institute_image" class="sr-only">Logo</label>
                    <input type="file" name="institute_image" id="institute_image" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('institute_image') border-red-500 @enderror" value="{{ old('institute_image', $institute->institute_image) }}/>
                    
                    @error('institute_image')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>                

                <div class="mb-4">
                    <label for="institute_courseType" class="sr-only">Institute Courses</label>
                    <input type="text" name="institute_courseType" id="institute_courseType" placeholder="Courses Offered" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('institute_courseType') border-red-500 @enderror" value="{{ old('institute_courseType',$institute->institute_courseType) }}">

                    @error('institute_courses')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div>
                    <input type="submit" class="bg-blue-500 text-white px-4 py-3 rounded font-medium items-center" value="Update Institute">
                </div>
            </form>

        </div>
    </div>    
@endsection
