@extends('layouts.adminapp')

@section('content')
<div class="bg-gray-100  mx-auto px-4 md:px-12 ">

<div class="container flex flex-col flex-wrap items-center justify-between py-5 mx-auto md:flex-row max-w-7xl">
    <div class="relative flex flex-col md:flex-row">
    <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">Institute Name</h1>
    </div>

    <div class="inline-flex items-center ml-5 space-x-6 lg:justify-end">
        
        <span class="inline-flex rounded-md shadow-sm">
        @if(auth()->user()->role_id == '1')
            <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">
                <a href="{{ route('institutes.edit',$institute->institute_id)}}" class="inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap transition duration-150 ease-in-out bg-indigo-600 border border-transparent rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700">
                Edit institute
                </a>
            </h1>

            <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">
            <form action="{{route('institutes.destroy',$institute->institute_id)}}" method="post">
                @csrf
                @method('DELETE')
                    <button class="inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap transition duration-150 ease-in-out bg-red-600 border border-transparent rounded-md hover:bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-indigo active:bg-indigo-700" onclick="confirmDelete()">
                        Delete Institute
                    </button>
            </form>
            </h1>            
            @endif
        </span>
    </div>
  </div>

  <div class="grid grid-cols-1 lg:grid-cols-4 gap-4 lg:gap-8 m-5">
  <div class="border p-4 text-center">
  <img alt="{{$institute->institute_name}}" class="block h-auto w-full" src="{{asset('images/institutes/' . $institute->institute_image)}}">
  </div>
  <div class="border p-4 text-center col-span-1 lg:col-span-2">
    <p>{{$institute->institute_name}}</p>
    <p>{{$institute->institute_description}}</p>
    <p>Programs Offered {{$institute->institute_courseType}}</p>
    
  </div>  
   </div>
</div>

<script type="text/javascript">
        function confirmDelete(){
            return confirm('Are you sure?');
        }
    </script>
@endsection