@extends('layouts.adminapp')

@section('content')
<div class="bg-gray-100  mx-auto px-4 md:px-12 ">

    <div class="container flex flex-col flex-wrap items-center justify-between py-5 mx-auto md:flex-row max-w-7xl">
        <div class="relative flex flex-col md:flex-row">
            <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">Course Name</h1>
        </div>

        <div class="inline-flex items-center ml-5 space-x-6 lg:justify-end">

            <span class="inline-flex rounded-md shadow-sm">
                @if(auth()->user()->role == '1')
                <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">
                    <a href="{{ route('courses.edit',$course->id)}}"
                        class="inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap transition duration-150 ease-in-out bg-indigo-600 border border-transparent rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700">
                        Edit Course
                    </a>
                </h1>

                <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">
                    <form action="{{route('courses.destroy',$course->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button
                            class="inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap transition duration-150 ease-in-out bg-red-600 border border-transparent rounded-md hover:bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-indigo active:bg-indigo-700"
                            onclick="confirmDelete()">
                            Delete Course
                        </button>
                    </form>
                </h1>
                @endif
                @if($course->course_author == auth()->user()->id or auth()->user()->role == '1')
                <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">
                    <a href="{{ route('add-lesson')}}"
                        class="inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap transition duration-150 ease-in-out bg-indigo-600 border border-transparent rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700">
                        Add Lesson
                    </a>
                </h1>
                @endif
            </span>
        </div>
    </div>

    <div class="grid grid-cols-1 lg:grid-cols-4 gap-4 lg:gap-8 m-5">
        <div class="border p-4 text-center">
            <img alt="{{$course->course_name}}" class="block h-auto w-full"
                src="{{asset('images/courses/' . $course->course_image)}}">
        </div>
        <div class="border p-4 col-span-1 lg:col-span-2">
            <p>{{$course->course_name}}</p>
            <p>{{$course->course_description}}</p>
            <p>Institute: {{$course->institute->institute_name}}</p>
            <p>Category: {{$course->course_category}}</p>
            <p>Education Level: {{$course->course_educationLevel}}</p>
            <p>Semester: {{$course->course_semester}}</p>
            <p>Course By {{$course->user->name}}</p>
            @if($course->course_isPremium == 1)
            <p>Course Price: {{$course->course_price}}</p>
            @else
            <p>This course is free</p>
            @endif

        </div>
    </div>
    <div class="p-4">
        <h4 class="text-lg text-333">Lessons</h4>
            <table class="table-auto">
                <thead>
                    <tr>
                        <th class="px-5">Lesson Name</th>
                        <th class="px-5">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($lessons as $lesson)
                    <tr>
                        <td class="px-5">{{$lesson->lesson_name}}</td>
                        <td class="px-5">
                            <div class="btn-group">
                                <a class="px-2" href="#" class="btn btn-sm">
                                    Edit
                                </a>
                                <a class="px-2" href="#" class="btn btn-sm" ">
                                    <form action="{{route('delete-lesson',$lesson->lesson_id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button
                                            class="inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap transition duration-150 ease-in-out bg-red-600 border border-transparent rounded-md hover:bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-indigo active:bg-indigo-700"
                                            onclick="confirmDelete()">
                                            Delete Lesson
                                        </button>
                                    </form>
                                    
                                </a>
                            </div>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>    
            
            
            
    </div>



    

</div>

<script type="text/javascript">
    function confirmDelete() {
        return confirm('Are you sure?');
    }

</script>
@endsection
