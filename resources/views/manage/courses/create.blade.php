@extends('layouts.adminapp')

@section('content')
    <div class="flex justify-center">
        <div class="w-8/12 bg-white p-6 rounded-lg">
            <h3>Add Course</h3>
            <form action="{{ route('courses.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="mb-4 mt-4">
                    <label for="course_name" class="sr-only">Course Name</label>
                    <input type="text" name="course_name" id="course_name" placeholder="Course name" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('course_name') border-red-500 @enderror" value="{{ old('course_name') }}">

                    @error('course_name')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="course_title" class="sr-only">Course Title</label>
                    <input type="text" name="course_title" id="course_title" placeholder="Course Title" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('course_title') border-red-500 @enderror" value="{{ old('course_title') }}">

                    @error('course_title')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="course_description" >Course Description</label>
                    <textarea name="course_description" id="course_descsription" placeholder="Course Description" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('course_description') border-red-500 @enderror" value="{{ old('course_description') }}">@error('course_description')<div class="text-red-500 mt-2 text-sm">{{ $message }}</div>@enderror</textarea>
                </div>

                <div class="mb-4">
                    <label for="course_image" class="sr-only">Image</label>
                    <input type="file" name="course_image" id="course_image" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('course_image') border-red-500 @enderror"/>
                    
                    @error('course_image')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="course_institute" class="sr-only">Course Institute</label>
                    <select name="course_institute" class="form-control">
                        <option value="">Select Institute</option>
                        @foreach ($institutes as $key => $value)
                            <option value="{{ $value->institute_id }}">{{ $value->institute_name }}</option>
                        @endforeach
                    </select>

                    @error('course_institute')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-4">        
                    <input type="checkbox" id="course_isPremium" name="course_isPremium" value="1" default="0" class="course_isPremium" onclick="coursePrice()">
                    <span class="text-gray-900 font-medium">Is course premium?</span>           
                    
                </div>

                <div class="mb-4" id="course-price-div" style="display:none;">
                    <label for="course_price" class="sr-only">Course Price</label>
                    <input type="text" name="course_price" id="course_price" placeholder="Course price" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('course_price') border-red-500 @enderror" value="{{ old('course_price') }}">

                    @error('course_price')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="course_category" class="sr-only">Course Category</label>
                    <input type="text" name="course_category" id="course_category" placeholder="Course Category (IT, CS, etc.)" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('course_category') border-red-500 @enderror" value="{{ old('course_category') }}">

                    @error('course_category')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="course_educationLevel" class="sr-only">Education Level</label>
                    <input type="text" name="course_educationLevel" id="course_educationLevel" placeholder="Course Level (Bsc IT, CS, Msc IT, etc.)" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('course_educationLevel') border-red-500 @enderror" value="{{ old('course_educationLevel') }}">

                    @error('course_educationLevel')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="course_semester" class="sr-only">Course Semester</label>
                    <input type="text" name="course_semester" id="course_semester" placeholder="Course Semester (1st, 2nd, etc.)" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('course_semester') border-red-500 @enderror" value="{{ old('course_semester') }}">

                    @error('course_semester')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div>
                    <input type="submit" class="bg-blue-500 text-white px-4 py-3 rounded font-medium items-center" value="Create Course">
                </div>
            </form>

        </div>
    </div>

    <script type="text/javascript">
        function coursePrice(){
            let coursePremium = document.getElementById('course_isPremium');
            let coursePrice = document.getElementById('course-price-div');
            if(coursePremium.checked == true){
                coursePrice.style.display = "block";
                
            }else{
                coursePrice.style.display = "none";
                
            }
        }
        coursePrice();
    </script>
@endsection
