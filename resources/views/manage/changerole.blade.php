@extends('layouts.adminapp')

@section('content')
<div class="flex justify-center">
    <div class="w-8/12 bg-white p-6 rounded-lg">
        <h3>Change Role</h3>
        <form action="{{ route('changeUserRole') }}" method="POST"">
            @csrf
            
            <div>
                <input type="submit" class="bg-blue-500 text-white px-4 py-3 rounded font-medium items-center" value="Change Role">
            </div>
        </form>

    </div>
</div>    
@endsection