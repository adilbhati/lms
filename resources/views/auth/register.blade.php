@extends('layouts.app')

@section('content')
<section class="w-full bg-white">

    <div class="mx-auto max-w-7xl">

        <div class="flex flex-col lg:flex-row">
            <div class="relative w-full bg-cover lg:w-6/12 xl:w-7/12 bg-gradient-to-r from-white via-white to-gray-100">

                <div class="relative flex flex-col items-center justify-center w-full h-full px-10 my-20 lg:px-16 lg:my-0">
                    <div class="flex flex-col items-start space-y-8 tracking-tight lg:max-w-3xl">
                        <div class="relative">
                            <p class="mb-2 font-medium text-gray-700 uppercase">Demo Text</p>
                            <h2 class="text-5xl font-bold text-gray-900 xl:text-6xl">Demo text. Demo text.</h2>
                        </div>
                        <p class="text-2xl text-gray-700">Demo text. Demo text. Demo text. Demo text. Demo text. Demo text. Demo text</p>
                        <a href="#_" class="inline-block px-8 py-5 text-xl font-medium text-center text-white transition duration-200 bg-blue-600 rounded-lg hover:bg-blue-700 ease">Get Started Today</a>
                    </div>
                </div>
            </div>

            <div class="w-full bg-white lg:w-6/12 xl:w-5/12">

                <div class="flex flex-col items-start justify-start w-full h-full p-10 lg:p-16 xl:p-24">
                    <h4 class="w-full text-3xl font-bold">Register</h4>
                    <p class="text-lg text-gray-500">or, if you have an account you can <a href="{{route('login')}}" class="text-blue-600 underline">sign in</a></p>
                    <div class="relative w-full mt-10 space-y-8">
                    <form action="{{route('register')}}" method="POST">
                        @csrf
                        <div class="relative">
                            <label class="font-medium text-gray-900">Name</label>
                            <input type="text" name="name" id="name" class="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50 @error('name') border-red-500 @enderror" value="{{ old('name') }}" placeholder="Enter Your Name" />
                            @error('name')
                                <div class="text-red-500 mt-2 text-sm">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="relative">
                            <label class="font-medium text-gray-900">Username</label>
                            <input type="text" name="username" id="username" class="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50 @error('username') border-red-500 @enderror" value="{{ old('username') }}" placeholder="Enter Your Username" />
                            @error('username')
                                <div class="text-red-500 mt-2 text-sm">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="relative">
                            <label class="font-medium text-gray-900">Email</label>
                            <input type="text" name="email" id="email" class="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50 @error('email') border-red-500 @enderror" value="{{ old('email') }}" placeholder="Enter Your Email Address" />
                            @error('email')
                                <div class="text-red-500 mt-2 text-sm">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="relative">
                            <label class="font-medium text-gray-900">Password</label>
                            <input type="password" name="password" id="password" class="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50 @error('password') border-red-500 @enderror" value="{{ old('password') }}" placeholder="Password" />
                            @error('password')
                                <div class="text-red-500 mt-2 text-sm">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="relative">
                            <label class="font-medium text-gray-900">Password Again</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50 @error('password_confirmation') border-red-500 @enderror" value="{{ old('password_confirmation') }}" placeholder="Password Again" />
                            @error('password_confirmation')
                                <div class="text-red-500 mt-2 text-sm">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="relative">
                            <label for="course_institute" class="font-medium text-gray-900">Institute</label>
                            <select name="course_institute" class="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50">
                                <option value="">Select Institute</option>
                                @foreach ($institutes as $key => $value)
                                    <option value="{{ $value->institute_id }}">{{ $value->institute_name }}</option>
                                @endforeach
                            </select>
        
                            @error('course_institute')
                                <div class="text-red-500 mt-2 text-sm">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="relative">
                            <button type="submit" class="inline-block w-full mt-4 px-5 py-4 text-lg font-medium text-center text-white transition duration-200 bg-blue-600 rounded-lg hover:bg-blue-700 ease">Create Account</button>
                           <!-- <a href="#_" class="inline-block w-full px-5 py-4 mt-3 text-lg font-bold text-center text-gray-900 transition duration-200 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 ease">Sign up with Google</a> -->
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

@endsection