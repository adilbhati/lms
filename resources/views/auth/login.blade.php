@extends('layouts.app')

@section('content')
<!------->
<section class="w-full px-8 py-16 bg-gray-100 xl:px-8">
    <div class="max-w-5xl mx-auto">
        <div class="flex flex-col items-center md:flex-row">

            <div class="w-full space-y-5 md:w-3/5 md:pr-16">
                <p class="font-medium text-blue-500 uppercase">Welcome to EduDev!</p>
                <h2 class="text-2xl font-extrabold leading-none text-black sm:text-3xl md:text-5xl">
                    Changing The Way You Learn.
                </h2>
                <p class="text-xl text-gray-600 md:pr-16">Demo text. Demo text. Demo text. Demo text. Demo text. Demo text. Demo text. Demo text. Demo text. Demo text. Demo text. Demo text. Demo text. </p>
            </div>

            <div class="w-full mt-16 md:mt-0 md:w-2/5">
            @if(session('status'))
                <div class="bg-red-500 p-4 rounded-lg mb-6 text-white text-center">
                {{session('status')}}
                </div>
            @endif

                <div class="relative z-10 h-auto p-8 py-10 overflow-hidden bg-white border-b-2 border-gray-300 rounded-lg shadow-2xl px-7">
                    <h3 class="mb-6 text-2xl font-medium text-center">Sign in to your Account</h3>
                    <form action="{{ route('login') }}" method="post">
                    @csrf
                        <div class="block mb-4 border border-gray-200 rounded-lg">
                            <input type="text" name="email" id="email" class="block w-full px-4 py-3 border-2 border-transparent rounded-lg focus:border-blue-500 focus:outline-none @error('email') border-red-500 @enderror" value="{{ old('email') }}" placeholder="Email address">

                            @error('email')
                                <div class="text-red-500 mt-2 text-sm">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="block mb-4 border border-gray-200 rounded-lg">
                            <input type="password" name="password" id="password" class="block w-full px-4 py-3 border-2 border-transparent rounded-lg focus:border-blue-500 focus:outline-none @error('password') border-red-500 @enderror" placeholder="Password">

                            @error('password')
                                <div class="text-red-500 mt-2 text-sm">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="block mb-4 border border-gray-200 rounded-lg">
                            <input type="checkbox" name="remember" id="remember" class="mr-2">
                            <label for="remember">Remember me</label>
                        </div>                        
                        <div class="block">
                            <button type="submit" class="w-full px-3 py-4 font-medium text-white bg-blue-600 rounded-lg">Log Me In</button>
                        </div>
                        <p class="w-full mt-4 text-sm text-center text-gray-500">Don't have an account? <a href="{{route('register')}}" class="text-blue-500 underline">Sign up here</a></p>
                    </form>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection