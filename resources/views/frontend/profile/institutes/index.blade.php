@extends('layouts.app')

@section('content')

<div class="bg-gray-100  mx-auto px-4 md:px-12 ">

<div class="container flex flex-col flex-wrap items-center justify-between py-5 mx-auto md:flex-row max-w-7xl">
    <div class="relative flex flex-col md:flex-row">
    <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">All Institutes</h1>
    </div>

    <div class="inline-flex items-center ml-5 space-x-6 lg:justify-end">
        
        
    </div>
  </div>


@if($institutes->count())
    <div class="flex flex-wrap -mx-1 lg:-mx-4">
        @foreach($institutes as $institute)
        <!-- Column -->
        <div class="my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3">

            <!-- Article -->
            <article class="overflow-hidden rounded-lg shadow-lg">

                <a href="#">
                    <img alt="{{$institute->institute_name}}" class="block h-auto w-full" src="{{asset('images/institutes/' . $institute->institute_image)}}">
                </a>

                <header class="flex items-center justify-between leading-tight p-2 md:p-4 bg-gray-200">
                    <h1 class="text-lg">
                        <a class="no-underline hover:underline text-black" href="#">
                            {{$institute->institute_name}}
                        </a>
                    </h1>
                </header>
                <p class="p-2">{{$institute->institute_description}}</p>
                <div class="flex items-center justify-between leading-tight p-2 md:p-2">
                    <p>Courses Offered: <strong>{{$institute->institute_courseType}}</strong></p>
                    <a href="{{URL::to('frontend/profile/institutes/show/'.$institute->institute_id)}}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Read More</a>
                </div>

            </article>
            <!-- END Article -->

        </div>
        <!-- END Column -->
        @endforeach
    </div>
@else
<p>There are no institutes available</p>
@endif
</div>
@endsection