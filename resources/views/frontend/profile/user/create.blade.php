@extends('layouts.adminapp')

@section('content')
    <div class="flex justify-center">
        <div class="w-8/12 bg-white p-6 rounded-lg">
            <h3 class="text-2xl text-grey-700 py-5">Complete Profile</h3>
            <form action="{{ route('storeUser') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="mb-4">
                    <label for="course_institute" class="sr-only">Course Institute</label>
                    <select name="course_institute" class="form-control">
                        <option value="">Select Institute</option>
                        @foreach ($institutes as $key => $value)
                            <option value="{{ $value->institute_id }}">{{ $value->institute_name }}</option>
                        @endforeach
                    </select>

                    @error('institute_name')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>            

                
                <div>
                    <input type="submit" class="bg-blue-500 text-white px-4 py-3 rounded font-medium items-center" value="Update Profile">
                </div>
            </form>

        </div>
    </div> 
@endsection
