@extends('layouts.app')

@section('content')
<div class="bg-gray-100  mx-auto px-4 md:px-12 ">

<div class="container flex flex-col flex-wrap items-center justify-between py-5 mx-auto md:flex-row max-w-7xl">
    <div class="relative flex flex-col md:flex-row">
    <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">All Courses</h1>
    </div>

    <div class="inline-flex items-center ml-5 space-x-6 lg:justify-end">
        
        <span class="inline-flex rounded-md shadow-sm">
        @if(auth()->user()->role_id == '1')
            <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">
                <a href="{{URL::to('manage/courses/create')}}" class="inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap transition duration-150 ease-in-out bg-indigo-600 border border-transparent rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700">
                Create Course
                </a>
            </h1>
            @endif
        </span>
    </div>
  </div>


@if($courses->count())
    <div class="flex flex-wrap -mx-1 lg:-mx-4">
        @foreach($courses as $course)
        <!-- Column -->
        <div class="my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3">

            <!-- Article -->
            <article class="overflow-hidden rounded-lg shadow-lg">

                <a href="#">
                    <img alt="{{$course->course_name}}" class="block h-auto w-full" src="{{asset('images/courses/' . $course->course_image)}}">
                </a>

                <header class="flex items-center justify-between leading-tight p-2 md:p-4 bg-gray-200">
                    <h1 class="text-lg">
                        <a class="no-underline hover:underline text-black" href="#">
                            {{$course->course_name}}
                        </a>
                    </h1>
                <p class="text-grey-darker text-sm">
                        {{$course->created_at->diffForHumans()}}
                </p>
                </header>
                <p class="p-2">{{$course->course_description}}</p>
                <div class="flex items-center justify-between leading-tight p-2 md:p-2">
                    <p>Course By: <strong>{{$course->user->name}}</strong></p>
                    <a href="{{URL::to('frontend/courses/show/'.$course->id)}}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Read More</a>
                </div>

            </article>
            <!-- END Article -->

        </div>
        <!-- END Column -->
        @endforeach
    </div>
@else
<p>There are no courses available</p>
@endif
</div>
@endsection