@extends('layouts.app')

@section('content')
<div class="bg-gray-100  mx-auto px-4 md:px-12 ">

<div class="container flex flex-col flex-wrap items-center justify-between py-5 mx-auto md:flex-row max-w-7xl">
    <div class="relative flex flex-col md:flex-row">
    <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">{{$course->course_name}}</h1>
    </div>

    <div class="inline-flex items-center ml-5 space-x-6 lg:justify-end">
        
        <span class="inline-flex rounded-md shadow-sm">
        @if(auth()->user()->role_id == '1')
            <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">
                <a href="{{ route('edit-course',$course->id)}}" class="inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap transition duration-150 ease-in-out bg-indigo-600 border border-transparent rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700">
                Edit Course
                </a>
            </h1>

            <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">
            <form action="{{route('delete-course',$course->id)}}" method="post">
                @csrf
                @method('DELETE')
                <button class="inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap transition duration-150 ease-in-out bg-red-600 border border-transparent rounded-md hover:bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-indigo active:bg-indigo-700">
                Delete Course
                </button>
            </form>
            </h1>            
        @endif
        @if($checkstudentCourse > 0 )
            <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">
           
                <button class="inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap transition duration-150 ease-in-out bg-red-600 border border-transparent rounded-md hover:bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-indigo active:bg-indigo-700">
                Subscribed
                </button>
            
            </h1>   
        @else
        <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">
                <form action="{{route('subscribe-course',$course->id)}}" method="post">
                    @csrf
                    
                    <button class="inline-flex items-center justify-center px-4 py-2 text-base font-medium leading-6 text-white whitespace-no-wrap transition duration-150 ease-in-out bg-red-600 border border-transparent rounded-md hover:bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-indigo active:bg-indigo-700">
                    Subscribe Course
                    </button>
                </form>
            </h1>   
        @endif
            
        </span>
    </div>
  </div>

  <div class="grid grid-cols-1 lg:grid-cols-4 gap-4 lg:gap-8 m-5">
  <div class="border p-4 text-center">
  <img alt="{{$course->course_name}}" class="block h-auto w-full" src="{{asset('images/courses/' . $course->course_image)}}">
  </div>
  <div class="border p-4 text-center col-span-1 lg:col-span-2">
    <p>{{$course->course_name}}</p>
    <p>{{$course->course_description}}</p>
    <p>Course By {{$course->user->name}}</p>
    
  </div>  

  
   
   </div>
   @if($checkstudentCourse > 0 )
    <div class="p-4">
        <h4 class="text-lg text-333">Lessons</h4>
    <ol class="list-decimal pl-4">
        @foreach($lessons as $lesson)
            <li><a href="{{ route('view-lesson',$lesson->lesson_id)}}">{{$lesson->lesson_name}}</a></li>
        @endforeach
    </div>
    @endif
</div>
@endsection