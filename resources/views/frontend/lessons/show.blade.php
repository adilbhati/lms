@extends('layouts.app')

@section('content')
<div class="bg-gray-100  mx-auto px-4 md:px-12 ">

<div class="container flex flex-col flex-wrap items-center justify-between py-5 mx-auto md:flex-row max-w-7xl">
    <div class="relative flex flex-col md:flex-row">
        <h1 class="sm:text-3xl text-2xl font-medium title-font p-1.5 text-black">{{$lesson->lesson_name}}</h1>
    </div>

  
</div>
<div class="container px-12">
    <div class="flex flex-wrap">
        <div>
            <div class="">
                <iframe width="850" height="375" src="https://www.youtube.com/embed/{{$lesson->lesson_video_url}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            
            </div>
            
        </div>
        <div class="pl-6">
            <h4 class="p-4 text-xl text-555">Lessons in this course</h4>
            @if($otherlessons->count())
                @foreach($otherlessons as $otherlesson)
                    <p>{{$otherlesson->lesson_name}}</p>
                @endforeach
            @else
            <p>There are no other lessons available</p>
            @endif
        </div>
        
    </div>
    <div class="py-4">
        <h4 class="text-grey-700 text-xl py-4">{{$lesson->lesson_description}}</h4>
        <p>{{$lesson->lesson_transcript}}</p>
    </div>
    <div class="py-4">
        Resources : <a class="text-blue-700" href="{{asset('documents/lessons/' . $lesson->lesson_document)}}">Download</a>
    </div>
</div>
@endsection