@extends('layouts.app')

@section('content')

    <div class="flex justify-center">
        <div class="w-8/12 bg-white p-6 rounded-lg">
            <h3>Add Course</h3>
            <form action="{{ route('store-course') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="mb-4 mt-4">
                    <label for="course_name" class="sr-only">Course Name</label>
                    <input type="text" name="course_name" id="course_name" placeholder="Course name" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('course_name') border-red-500 @enderror" value="{{ old('course_name') }}">

                    @error('course_name')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="course_title" class="sr-only">Course Title</label>
                    <input type="text" name="course_title" id="course_title" placeholder="Course Title" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('course_title') border-red-500 @enderror" value="{{ old('course_title') }}">

                    @error('course_title')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="course_description" class="sr-only">Course Description</label>
                    <input type="text" name="course_description" id="course_descsription" placeholder="Course Description" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('course_description') border-red-500 @enderror" value="{{ old('course_description') }}">

                    @error('course_description')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="course_image" class="sr-only">Image</label>
                    <input type="file" name="course_image" id="course_image" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('course_image') border-red-500 @enderror"/>
                    
                    @error('course_image')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="mb-4">        
                    <input type="checkbox" id="course_isPremium" name="course_isPremium" value="1" class="course_isPremium" onclick="coursePrice()">
                    <span class="text-gray-900 font-medium">Is course premium?</span>           
                    
                </div>

                <div class="mb-4" id="course-price-div" style="display:none;">
                    <label for="course_price" class="sr-only">Course Price</label>
                    <input type="text" name="course_price" id="course_price" placeholder="Course price" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('course_price') border-red-500 @enderror" value="{{ old('course_price') }}">

                    @error('course_price')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div>
                    <input type="submit" class="bg-blue-500 text-white px-4 py-3 rounded font-medium items-center" value="Create Course">
                </div>
            </form>

        </div>
    </div>

    <script type="text/javascript">
        function coursePrice(){
            let coursePremium = document.getElementById('course_isPremium');
            let coursePrice = document.getElementById('course-price-div');
            if(coursePremium.checked == true){
                coursePrice.style.display = "block";
            }else{
                coursePrice.style.display = "none";
            }
        }
        coursePrice();
    </script>

@endsection
