<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $primaryKey = 'role_id';

    const CREATED_AT = 'role_created';
    const UPDATED_AT = 'role_updated';

    protected $fillable = [
        'role_name',
        
    ];

    public function user()
    {
        return $this->belongsToMany(User::class);
    }
}
