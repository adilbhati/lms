<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Institute extends Model
{
    use HasFactory;

    protected $primaryKey = 'institute_id';

    const CREATED_AT = 'institute_created';
    const UPDATED_AT = 'institute_updated';

    protected $fillable = [
        'institute_name',
        'institute_description',
        'institute_image',
        'institute_courseType',
        'institute_email',
        'institute_courses',
    ];

    public function course()
    {
        return $this->hasMany(Course::class,'course_institute');
    }
}
