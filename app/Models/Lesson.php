<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    use HasFactory;

    protected $primaryKey = 'lesson_id';

    const CREATED_AT = 'lesson_created';
    const UPDATED_AT = 'lesson_updated';

    protected $fillable = [
        'lesson_name',
        'lesson_duration',
        'lesson_description',
        'lesson_image',
        'lesson_document',
        'lesson_video_url',
        'lesson_transcript',
        'lesson_course',
        'lesson_author'
        
        
    ];
}
