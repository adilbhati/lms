<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    protected $fillable = [
        'course_name',
        'course_title',
        'course_description',
        'course_author',
        'course_image',
        'course_isPremium',
        'course_price',
        'course_institute',
        'course_category',
        'course_educationLevel',
        'course_semester'
        
    ];


    public function user()
    {
        return $this->belongsTo(User::class,'course_author');
    }

    /*public function user()
    {
        return $this->belongsToMany(User::class);
    }*/

    public function institute()
    {
        return $this->belongsTo(Institute::class,'course_institute');
    }
}
