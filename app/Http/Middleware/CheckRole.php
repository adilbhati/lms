<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth; // laravel authentication
use Closure;
use Illuminate\Http\Request;


class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        if(!($user->role_id == '1')){
            abort('401');
        }
        return $next($request);
    }
}
