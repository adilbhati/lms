<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth; // laravel authentication

use Closure;
use Illuminate\Http\Request;

class ClearanceMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /* get request, check role and process the request*/
        $user = Auth::user();
        
        if ($request->routeIs('dashboard')) {
            /*if ($user->role_id == '1' or $user->role == '3') {
                return $next($request);                
            } 
            else {
                abort('401');
                
            }*/
            
        }

        
        return $next($request);
    }
}
