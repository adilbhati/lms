<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Course;
use App\Models\Lesson;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Auth;
use DB;



class AdminLessonController extends Controller
{
    public function addLesson(){
        //$course = Course::where('course_id',$id)->get();
        //dd('Hi');
        //Get User where user id is admin or moderator or institute_admin
        //Get current user id and role
        //if current user id matches role admin/moderator/institute_admin == get course list from current user institute
        
        
        $user = auth()->user();
        $id = $user->id;
        $userInstitute = DB::table('user_institute')->where('user_id','=',$id)->get();
        
        //dd($userInstitute);
        
            //if course institute matches user institute
        //$courses = Course::select('course_institute','course_name','id')->where('course_institute','=',$userInstitute)->get();
        $courses = Course::select('course_institute','course_name','id')->get();
        $coursestoshow=[];
        //$coursestoshow = array();
        //$coursestoshow = (array)null;
        for($i=0;$i<count($courses);$i++){
            //do something
            //dd($courses[$i]->id);
            if($courses[$i]->course_author = $id){
                array_push($coursestoshow,$courses[$i]);
            }

            
        }
        
        return view('manage.lessons.create',compact('coursestoshow'));
    }

    public function storeLesson(Request $request){
        $user = auth()->user();
        $id = $user->id;
        $role = $user->role;
        
        /*$request->validate([
            'lesson_name'=>'required|max:255',
            'lesson_duration'=>'max:1048',
            'lesson_description'=>'required|max:2048',
            'lesson_image'=>'mimes:jpg,png,gif|max:2048',
            'lesson_document'=>'mimes:pdf,docx,ppt|max:2048',
            'lesson_video_url'=>'required|max:255',
            'lesson_transcript'=>'required|max:10000',
            'lesson_course'=>'required|max:10',
        ]);*/
        if (!($request->lesson_image == NULL)){
            $image = $request->lesson_image;
            $imageName = $request->lesson_name.'.'.$request->lesson_image->extension();  
            $request->lesson_image->move(public_path('images/lessons'),$imageName);
        }
        else{
            $imageName = '';
        }

        if (!($request->lesson_document == NULL)){
            $doc = $request->lesson_document;
            $docName = $request->lesson_name.'.'.$request->lesson_document->extension();  
            $request->lesson_document->move(public_path('documents/lessons'),$docName);
        }
        else{
            $docName = '';
        }


        if($role == '1' or $role == '2' or $role == '3'){
            $lesson = Lesson::create([
                'lesson_name' =>request()->input('lesson_name'),
                'lesson_duration' =>request()->input('lesson_duration'),
                'lesson_description' =>request()->input('lesson_description'),
                'lesson_image' =>$imageName,
                'lesson_document' =>$docName,
                'lesson_video_url' =>request()->input('lesson_video_url'),
                'lesson_transcript' =>request()->input('lesson_transcript'),
                'lesson_course' =>request()->input('lesson_course'),
                'lesson_author' =>$id,
            ]);

            $lesson->save();
            return view('manage/courses');
        }
        else{
            abort(403, 'Unauthorized action.');
        }
        
    }

    public function getLesson($id){
        $lesson = Lesson::where('lesson_id',$id)->first();

        $otherlessons = Lesson::where('lesson_course',$lesson->lesson_course)->get();
        
        return view('frontend.lessons.show',compact('lesson','otherlessons'));


    }

    public function destroyLesson($id)
    {
        $lesson = Lesson::findOrFail($id);
        //dd($course);
        $lesson->delete();

        return redirect('manage/courses')->with('message','Lesson deleted successfully');
    }
}
