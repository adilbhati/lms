<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Institute;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Auth;

class AdminInstituteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $institutes = Institute::all();
        
        
        return view('manage.institutes.index', compact('institutes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manage.institutes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'institute_name'=>['required','unique:institutes','max:255'],            
            'institute_description'=>['required','max:2048'],
            'institute_image'=>['mimes:jpg,png,gif','max:2048'],
            'institute_courseType'=>['required','max:2048'],
            'institute_email'=>['required','max:255']
        ]);

                
        $image = $request->institute_name;
        
        $imageName = $image.'.'.$request->institute_image->extension();
        //dd($imageName);
        $request->institute_image->move(public_path('images/institutes'),$imageName);
        //dd('done');

        

        $institute = Institute::create([
            'institute_name' => request()->input('institute_name'),
            'institute_description' => request()->input('institute_description'),            
            'institute_image' => $imageName,
            'institute_courseType' => request()->input('institute_courseType'),
            'institute_email'=>request()->input('institute_email'),
            'institute_courses'=>'0'
            
        ]);

        $institute->save();
        

        return redirect('/manage/institutes')->with('message','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $institute = Institute::findOrFail($id);
        //dd($course);
        return view('manage.institutes.show', compact('institute'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $institute = Institute::findOrFail($id);
        //dd($course);
        return view('manage.institutes.edit', compact('institute'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $institute = Institute::findOrFail($id);
        $instituteImage = $institute->institute_image;
        //dd($courseImage);
        request()->validate([
            
            'institute_name'=>['required','max:255'],            
            'institute_description'=>['required','max:2048'],
            'institute_image'=>['mimes:jpg,png,gif','max:2048'],
            'institute_courseType'=>['required','max:2048'],
        ]);
        
        if($request->institute_image != ''){
            $path = public_path('images/institutes/');
            //Remove old file
            if($institute->institute_image != '' && $institute->institute_image != null){
                $file_old = $path.$institute->institute_image;
                unlink($file_old);
            }

            //upload new file
            $image = $request->institute_name;
        
            $imageName = $image.'.'.$request->institute_image->extension();
            //dd($imageName);
            $request->institute_image->move(public_path('images/institutes'),$imageName);
            $institute->institute_image = $imageName;
            
        }

        $institute->institute_name = $request->get('institute_name');        
        $institute->institute_description = $request->get('institute_description');
        $institute->institute_courseType = $request->get('institute_courseType');
        //$course->course_image = $imageName;
        
        $institute->update();

        return redirect('/manage/institutes')->with('message','institute updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
