<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\Institute;
use App\Models\Lesson;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Auth;

class AdminCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::with('user')->get();
        
        //dd($courses);
        return view('manage.courses.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $institutes = Institute::select('institute_name','institute_id')->get();
        //dd($institutes);
        return view('manage.courses.create',compact('institutes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        request()->validate([
            'course_name'=>['required','unique:courses','max:255'],
            'course_title'=>['required','max:255'],
            'course_description'=>['required','max:2048'],
            'course_image'=>['mimes:jpg,png,gif','max:2048'],
            'course_isPremium'=>['max:1'],
            'course_price'=> ['max:255'],['numeric'],
            'course_institute'=>['required'],
            'course_category'=>['max:2048'],
            'course_educationLevel'=>['max:2048'],
            'course_semester'=>['max:2048'],
        ]);

        
        
        //dd($request->course_isPremium);
        $image = $request->course_name;
        
        $imageName = $image.'.'.$request->course_image->extension();
        //dd($imageName);
        $request->course_image->move(public_path('images/courses'),$imageName);
        //dd('done');

        

        $course = Course::create([
            'course_name' => request()->input('course_name'),
            'course_title' => request()->input('course_title'),
            'course_description' => request()->input('course_description'),
            'course_author' => request()->user()->id,            
            'course_image' => $imageName,
            'course_isPremium' => request()->input('course_isPremium','0'),
            'course_price' => request()->input('course_price'),
            'course_institute' => request()->input('course_institute'),
            'course_category' => request()->input('course_category'),
            'course_educationLevel' => request()->input('course_educationLevel'),
            'course_semester' => request()->input('course_semester'),
            
        ]);

        $course->save();
        

        return redirect('/manage/courses')->with('message','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::findOrFail($id);
        $lessons = Lesson::where('lesson_course',$id)->get();
        //dd($course->course_price);
        return view('manage.courses.show', compact('course','lessons'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::findOrFail($id);
        $institutes = Institute::select('institute_name','institute_id')->get();
        //dd($course);
        return view('manage.courses.edit', compact('course','institutes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        $courseImage = $course->course_image;
        //dd($courseImage);
        request()->validate([
            
            'course_name'=>['required','max:255'],
            'course_title'=>['required','max:255'],
            'course_description'=>['required','max:2048'],
            'course_image'=>['mimes:jpg,png,gif','max:2048'],
            'course_isPremium'=>['max:1'],
            'course_price'=> ['max:255'],['numeric'],
            'course_institute'=>['required'],
            'course_category'=>['max:2048'],
            'course_educationLevel'=>['max:2048'],
            'course_semester'=>['max:2048'],
        ]);
        
        if($request->course_image != ''){
            $path = public_path('images/courses/');
            //Remove old file
            if($course->course_image != '' && $course->course_image != null){
                $file_old = $path.$course->course_image;
                unlink($file_old);
            }

            //upload new file
            $image = $request->course_name;
        
            $imageName = $image.'.'.$request->course_image->extension();
            //dd($imageName);
            $request->course_image->move(public_path('images/courses'),$imageName);
            $course->course_image = $imageName;
            
        }

        $course->course_name = $request->get('course_name');
        $course->course_title = $request->get('course_title');
        $course->course_description = $request->get('course_description');
        //$course->course_image = $imageName;
        $course->course_isPremium = $request->get('course_isPremium','0');
        $course->course_price = $request->get('course_price');
        $course->course_institute = $request->get('course_institute');
        $course->course_category = $request->get('course_category');
        $course->course_educationLevel = $request->get('course_educationLevel');
        $course->course_semester = $request->get('course_semester');


        $course->update();

        return redirect('/manage/courses')->with('message','course updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::findOrFail($id);
        //dd($course);
        $course->delete();

        return redirect('manage/courses')->with('message','Course deleted successfully');
    }
}
