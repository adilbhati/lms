<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Institute;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware(['guest']);
    }
    
    public function index()
    {
        $institutes = Institute::select('institute_name','institute_id')->get();
        return view('auth.register',compact('institutes'));
    }

    public function store(Request $request)
    {
        //dd($request);
        $this->validate($request, [
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed',
        ]);

        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => '4',
            'designation' => ''
        ]);

        DB::table('user_institute')->insert(array('user_id'=>$user->id,'institute_id'=>$request->course_institute,"created_at" =>  date('Y-m-d H:i:s'),"updated_at" => date('Y-m-d H:i:s')));
        //dd($user->id);


        auth()->attempt($request->only('email', 'password'));

        return redirect()->route('home');
    }
}
/*
While logging in, a user will have to select the institute to which they belong. Upon selecting, basic user details will be stored in the users table, while institute details of the user will be in user_institute table. Only Admin will be able to change the permission of the user. So he can make someone institute admin to any user. 
 */