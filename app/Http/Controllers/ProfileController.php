<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Institute;
use App\Models\Course;
use App\Models\User;
use App\Models\Role;
use Auth;

class ProfileController extends Controller
{
    public function instituteIndex()
    {                
        $institutes = Institute::get();
        return view('frontend.profile.institutes.index',compact('institutes'));
    }

    public function showInstitute($id)
    {
        $institute = Institute::findOrFail($id);        
        $courses = Course::where('course_institute',$id)->get();
        //dd($courseCount);
        return view('frontend.profile.institutes.show', compact('institute','courses'));
    }

    public function showUser($id)
    {
        $user = User::findOrFail($id);
        
        if($user->role_id == '1'){
            $courses = Course::where('course_author',$id)->get();
            //dd($course);       
        }
        elseif($user->role_id == '3'){
            $courses = Course::where('course_author',$id)->get();
            //dd($course);
        }
        else{
            $courses = Course::all();
        }
    
        //dd($courseCount);
        return view('frontend.profile.user.show', compact('user','courses'));
    }

    public function createUser(){
        $id = Auth::id(); 
        //get user id and institute list. add in user role and update user table
        $institutes = Institute::select('institute_id','institute_name')->get();
        //dd($institute);

        return view('frontend.profile.user.create',compact('institutes'));
    }

    public function storeUser(Request $request){
        $id = Auth::id();
        //dd($request->course_institute);
        $student_id = Role::where('role_name','Student')->first();
        $student_id = $student_id->role_id;
        //dd(Auth::user()->role());
        dd($student_id);
        Auth::user()->role()->attach($student_id);
        return redirect()->back();
    }

    /*public function showSubscribedCourses(){
        $id = Auth::id();
        $studentCourses = DB('student_course')->where()
    }*/
}
