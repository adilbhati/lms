<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Course;
use App\Models\Institute;
use Illuminate\Support\Facades\DB;

use Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','clearance']);
    }
    
    public function index()
    {
        $studentCount = User::where('role','2')->count();
        $moderatorCount = User::where('role','3')->count();
        $teacherCount = User::where('role','4')->count();
        $instituteAdminCount = User::where('role','5')->count();
        $courseCount = Course::count();
        $instituteCount = Institute::count();
        //dd($studentCount);
        return view('manage.dashboard',compact('studentCount','courseCount','instituteCount','moderatorCount','teacherCount','instituteAdminCount'));
    }

    public function changeRole()
    {
        //If logged in user is admin get all users and institutes
        //if logged in user belongs to a specific institute, get only those users who belongs to that institute        
        /*$id = Auth::user();
        if($id->role_id == '1'){
            $users = User::select('name','id')->get();    
        }
        else if($id->role_id == '5'){
            $user = User::select('name','id')->where('user_institute_id','=',$id->user_id)
        }*/
        
        
        return view('manage.changerole');
    }
}
