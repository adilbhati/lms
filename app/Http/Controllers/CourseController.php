<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\Institute;
use App\Models\Lesson;
use App\Models\User;
use App\Models\StudentCourse;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Auth;

class CourseController extends Controller
{
    public function __construct(){
        $this->middleware(['auth']);
    }

    public function index()
    {
        $courses = Course::with('user')->get();
        
        //dd($courses);
        return view('frontend.courses.index', compact('courses'));
    }

    public function create()
    {
        return view('frontend.courses.create');
    }

    public function store(Request $request)
    {
        request()->validate([
            'course_name'=>['required','unique:courses','max:255'],
            'course_title'=>['required','max:255'],
            'course_description'=>['required','max:2048'],
            'course_image'=>['mimes:jpg,png,gif','max:2048'],
            'course_isPremium'=>['max:1'],
            'course_price'=> ['max:255'],['numeric'],
        ]);
        $image = $request->course_name;
        
        $imageName = $image.'.'.$request->course_image->extension();
        //dd($imageName);
        $request->course_image->move(public_path('images/courses'),$imageName);
        //dd('done');
        $course = Course::create([
            'course_name' => request()->input('course_name'),
            'course_title' => request()->input('course_title'),
            'course_description' => request()->input('course_description'),
            'course_author' => request()->user()->id,            
            'course_image' => $imageName,
            'course_isPremium' => request()->input('course_isPremium'),
            'course_price' => request()->input('course_price'),
            
        ]);

        $course->save();
        

        return redirect('/frontend/courses')->with('message','success');
    }

    public function show($id)
    {
        $course = Course::findOrFail($id);
        //dd($course);
        $userId = Auth::user()->id;

        $lessons = Lesson::where('lesson_course',$id)->get();

        $studentSubscribed;
        $checkstudentCourse = StudentCourse::where('user_id',$userId)->where('course_id',$id)->count();
        

        return view('frontend.courses.show', compact(['course','checkstudentCourse','lessons']));
    }

    public function edit(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        return view('frontend.courses.edit', compact('course'));
        //dd($course);
    }

    public function update(Request $request, $id)
    {
        
        $course = Course::findOrFail($id);
        $courseImage = $course->course_image;
        //dd($courseImage);
        request()->validate([
            
            'course_name'=>['required','max:255'],
            'course_title'=>['required','max:255'],
            'course_description'=>['required','max:2048'],
            'course_image'=>['mimes:jpg,png,gif','max:2048'],
            'course_isPremium'=>['max:1'],
            'course_price'=> ['max:255'],['numeric'],
        ]);
        
        if($request->course_image != ''){
            $path = public_path('images/courses');
            //Remove old file
            if($course->course_image != '' && $course->course_image != null){
                $file_old = $path.$course->course_image;
                unlink($file_old);
            }

            //upload new file
            $image = $request->course_name;
        
            $imageName = $image.'.'.$request->course_image->extension();
            //dd($imageName);
            $request->course_image->move(public_path('images/courses'),$imageName);
            $course->course_image = $imageName;
            
        }

        $course->course_name = $request->get('course_name');
        $course->course_title = $request->get('course_title');
        $course->course_description = $request->get('course_description');
        //$course->course_image = $imageName;
        $course->course_isPremium = $request->get('course_isPremium');
        $course->course_price = $request->get('course_price');

        $course->update();

        return redirect('frontend.courses.index')->with('success', 'Course updated!');
    }

    public function destroy($id)
    {
        $course = Course::findOrFail($id);
        //dd($course);
        $course->delete();

        return redirect()->route('course', with('message','course deleted successfully'));
    }

    public function subscribe($id)
    {
        $userId = Auth::user()->id;

        $studentCourse = new StudentCourse;
        $studentCourse->user_id = $userId;
        $studentCourse->course_id = $id;

        $studentCourse->save();

        return redirect('/frontend/courses')->with('message','success');
        //dd($userId);
    }

    /*public function checkSubscription($id){
        $userId = Auth::user()->id;

        $checkstudentCourse = StudentCourse::where('user_id',$userId)->where('course_id',$id)->get();
        
        dd($checkstudentCourse);
    }*/
}
